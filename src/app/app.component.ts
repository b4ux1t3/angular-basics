import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  visible = true;

  clicks: number[];
  ngOnInit(): void {
    this.clicks = [];
  }

  toggle(): void {
    this.visible = !this.visible;
    this.clicks.push(this.clicks.length + 1);
  }
}
